-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2018 at 09:56 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE
= "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT
= 0;
START TRANSACTION;
SET time_zone
= "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dam1`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books`
(
  `book_id` int
(11) NOT NULL,
  `book_isbn` bigint
(13) UNSIGNED NOT NULL,
  `book_title` varchar
(50) NOT NULL,
  `book_author` varchar
(50) NOT NULL,
  `book_category` varchar
(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`,`book_isbn`, `book_title`, `book_author`, `book_category`) VALUES
(1, 7898277083, 'Pro Git', 'Scott Chacon', 'Programming'),
(2, 9926520565, 'jQuery in Action - Part2', 'Bear Bibeault', 'Programming'),
(3, 1234567890, 'Intro to Psychology', 'Ayesha Khan', 'Psychology'),
(4, 3858657958, 'Calculus for Beginners', 'John doe Sr.', 'Maths'),
(6, 8132828320, 'Advance Maths ', 'Prof. R. D. Sharma', 'Maths'),
(7, 8085657925, 'Javascript for begginners', 'Aurelio De Rosa ', 'Programming'),
(8, 9479473869, 'iOS App ', 'Ehtesham Mehmood', 'Programming'),
(9, 7581970439, 'Advance Physics', 'Resnik-Haliday', 'Physics'),
(10, 9893924939, 'HTML for dummies', 'Ehtesham Shami', 'Programming'),
(12, 9926520567, 'Intro to CodeIgniter Framework ', 'Ryan Benedett', 'Programming'),
(13, 9781847192561, 'Object Oriented Prgramming in PHP5', 'Hasin Hayder', 'Programming'),
(14, 9781449393212, 'Head First jQuery - Part 1', 'Ryan Benedetti', 'Programming'),
(16, 9780074655066, 'Introduction To Psychology', 'John R. Weisz', 'Psychology'),
(17, 9783450407140, 'Hidden Powers of Polynomials', 'Victor V. Proslov', 'Maths'),
(18, 8120818199, 'Vedic Maths', 'J.T. Glover', 'Maths'),
(19, 5210378104, 'The Evolution of Physics - Part 1', 'Albert Einstein', 'Physics'),
(20, 7484637160, 'Maths: Key Stage 2', 'Sean McArdle', 'Maths'),
(21, 1743149576, 'Summit Maths - Part 20', 'Ray Allen', 'Maths'),
(22, 9780330537230, 'The Dark of Sun', 'Wilbur Smith ', 'Adventure'),
(23, 9781447267140, 'A Falcon Flies', 'Wilbur Smith ', 'Historical Fiction, Adventure fiction'),
(24, 9781509864096, 'I Spy', 'Sidgwick - Jackson', 'Biography, Non Fiction'),
(25, 9781509874330, 'Long Road to Mercy', 'David Baldacci', ' Adventure fiction'),
(26, 9781509899463, 'Children of Virtue and Vengeance', 'Tomi Adeyemi', 'Historical Fiction, Adventure fiction'),
(27, 9781509859689, 'The Hit', 'David Baldacci', 'Adventure fiction'),
(NULL, 1234567891234, 'Book title', 'Book Author', 'Book Category');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
ADD PRIMARY KEY
(`book_id`),
ADD UNIQUE KEY `book_isbn`
(`book_isbn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int
(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
