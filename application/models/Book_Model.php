<?php

class Book_Model extends CI_Model {

    private $tableName = "books";
    
    public function getPageCount($searchTerm, $rec_per_pg){
        $this->db->like('book_title',$searchTerm);
        $this->db->or_like('book_isbn',$searchTerm);
        $this->db->or_like('book_author',$searchTerm);
        $this->db->or_like('book_category',$searchTerm);
        
        $query = $this->db->get($this->tableName);
        
        //sending the result of query back to the controller
        return ceil($query->num_rows() / $rec_per_pg);
    }

    public function getAllBooks($searchTerm, $page, $rec_per_page) {
         
        $limit  = $rec_per_page;
        $offset = ($page - 1)*$rec_per_page;
        
        //SELECT * FROM books WHERE 'book_title' LIKE %$searchTerm% OR
        // 'book_author' LIKE %$searchTerm% OR
        // 'book_category' LIKE %$searchTerm% OR
        // 'book_isbn' LIKE %$searchTerm% LIMIT $limit, OFFSET $offset
        
        $this->db->like('book_title',$searchTerm);
        $this->db->or_like('book_isbn',$searchTerm);
        $this->db->or_like('book_author',$searchTerm);
        $this->db->or_like('book_category',$searchTerm);
        $this->db->limit($limit, $offset);
        $query = $this->db->get($this->tableName);
        
        //sending the result of query back to the controller
        return $query->result();
    }
}
