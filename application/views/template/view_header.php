<!DOCTYPE html>

<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.css')?>">
        
        <!-- Custom Style sheet -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/appStyle.css')?>">
    </head>
    <body>
        