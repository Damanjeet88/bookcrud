<div class="container">
    <div class="row">
        <div class="col-sm text-center my-3">
            <h2 class="bg-dark text-white p-2 font-weight-light">Books CRUD</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <form action="<?php echo site_url('book') ?>" method="GET">
                <div class="input-group mb-3">
                    <input value="<?php echo $searchTerm ?>" type="text" class="form-control" name="searchTerm" placeholder="Search" aria-label="Search Term" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-dark" type="submit">Search</button>
                        <a href="<?php echo site_url('book') ?>" class="btn btn-outline-secondary">Clear</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6"></div>
    </div>
    <div class="row">
        <div class="col-sm">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Book Title</th>
                        <th>Author</th>
                        <th>Category</th>
                        <th>ISBN</th>
                        <th class="text-center">Action</th>
                    </tr>   
                </thead>
                <tbody>
                    <?php if ($list): ?>
                        <!--If $list contains any records, print them-->
                        <?php foreach ($list as $book): ?>
                            <tr>
                                <td><?php echo $book->book_title ?></td>    
                                <td><?php echo $book->book_author ?></td>    
                                <td><?php echo $book->book_category ?></td>    
                                <td><?php echo $book->book_isbn ?></td>    
                                <td>
                                    <a href="" class="btn btn-success">Update</a>
                                    <a href="" class="btn btn-outline-dark">Delete</a>
                                </td>    
                            </tr>
                        <?php endforeach; ?>
                        <!--else print a 'No data found' message-->
                    <?php else: ?>
                        <tr>
                            <td colspan="5" class="text-center">No data found</td>    
                        </tr>
                    <?php endif; ?>

                <td></td>

                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-sm d-flex" style="justify-content: center">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php $i = 1 ?>
                    <li class="page-item <?php echo ($page == 1)?'disabled':''?>">
                        <a href="<?php echo site_url('book') . '?action=prev&searchTerm=' . $searchTerm . '&pg=' . $page ?>"  
                           class="page-link font-weight-bold">Prev</a>
                    </li>
                    <?php for ($i = 1; $i <= $page_count; $i++): ?>
                        <li class="page-item <?php echo ($page == $i) ? 'active' : '' ?>">
                            <a class="page-link" 
                               href="<?php echo site_url('book') . '?searchTerm=' . $searchTerm . '&pg=' . $i ?>">
                                   <?php echo $i ?>
                            </a>
                        </li>
                    <?php endfor; ?>
                    <li class="page-item <?php echo ($page == $page_count)?'disabled':''?>">
                        <a href="<?php echo site_url('book') . '?action=next&searchTerm=' . $searchTerm . '&pg=' . $page ?>"
                           class="page-link font-weight-bold" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

</div>

