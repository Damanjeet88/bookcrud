<?php

class Book extends MY_Controller {

    public function index() {
        //recieving user input from $_GET array in codeIgniter style
        $searchTerm = $this->input->get('searchTerm');

        // getting page from $_GET array i.e. from URL
        $page = $this->input->get('pg');

        //records to be displayed in a page
        $rec_per_page = 5;
        $page_count = $this->Book_Model->getPageCount($searchTerm, $rec_per_page);

        //handling prev & next button
        $action = $this->input->get('action');
        if (isset($action) && !empty($action)) {
            if ($action == 'prev') {
                $page = $page - 1;
            }
            if ($action == 'next') {
                $page = $page + 1;
            }
        }

        //preventing illegal page values through URL
        if (!isset($page) || empty($page)) {
            $page = 1;
        }
        if ($page > $page_count) {
            $page = $page_count;
        }
        if ($page < 1) {
            $page = 1;
        }

        //passing the search term (user Input) to model method getAllBooks()
        $list = $this->Book_Model->getAllBooks($searchTerm, $page, $rec_per_page);

        //loading different parts of pages: header, books list as a table, footer 
        $this->load->view('template/view_header');
        $this->load->view('view_dashboard', array(
            'list' => $list,
            'searchTerm' => $searchTerm,
            'page' => $page,
            'page_count' => $page_count)
        );
        $this->load->view('template/view_footer');
    }

}
